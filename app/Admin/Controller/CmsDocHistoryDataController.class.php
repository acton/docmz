<?php

namespace Admin\Controller;

class CmsDocHistoryDataController extends CmsController
{
    // 导出菜单
    // cmslist、cmshandle为必须要到导出的字段
    static $export_menu = array(
        'content' => array(
            '用户文档历史' => array(
                'cmslist' => array(
                    'title' => '文档列表',
                    'hiddens' => array(
                        'cmshandle' => '文档管理'
                    )
                )
            )
        )
    );
    // 标识字段，该字段为自增长字段
    public $cms_pk = 'id';
    // 数据表名称
    public $cms_table = 'cms_doc_history_data';
    // 数据库引擎
    public $cms_db_engine = 'MyISAM';
    // 列表列出的列出字段
    public $cms_fields_list = array(
        'id',
        'doc_id',
        'update_time',
        'content'
    );
    // 添加字段，留空表示所有字节均为添加项
    public $cms_fields_add = array();
    // 编辑字段，留空表示所有字节均为编辑项
    public $cms_fields_edit = array();
    // 搜索字段，表示列表搜索字段
    public $cms_fields_search = array();
    // 数据表字段
    public $cms_fields = array(

        'doc_id' => array(
            'title' => '文档ID',
            'description' => '',
            'type' => 'number',
            'default' => '0',
            'rules' => 'searchable|required|unsigned|readonly'
        ),

        'update_time' => array(
            'title' => '时间',
            'description' => '',
            'type' => 'datetime',
            'default' => '0',
            'rules' => 'searchable'
        ),

        'content' => array(
            'title' => 'Markdown内容',
            'description' => '',
            'type' => 'bigtext',
            'default' => '',
            'rules' => ''
        ),

    );
}
