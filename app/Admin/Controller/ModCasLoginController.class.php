<?php

namespace Admin\Controller;

use Home\Service\WordpressSyncerService;
use Think\Db;

class ModCasLoginController extends ModController
{
    static $export_menu = array(
        'system' => array(
            '系统设置' => array(
                'config' => array(
                    'title' => 'Cas登录',
                    'hiddens' => array()
                ),
            )
        )
    );

    public function build($yummy = false)
    {
        parent::build($yummy);

        switch ($this->build_db_type) {
            case 'mysql' :
                $sqls = array();
                break;
        }

        foreach ($sqls as $sql) {
            $this->build_db->execute($sql);
        }

        parent::build_success($yummy);
    }

    public function config()
    {
        $keys = array(
            'cas_login_enable',
            'cas_login_host',
            'cas_login_port',
            'cas_login_path',
        );
        if (IS_POST) {
            foreach ($keys as &$k) {
                tpx_config($k, I('post.' . $k, '', 'trim'));
            }
            $this->success('保存成功');
        }

        foreach ($keys as &$k) {
            $this->$k = tpx_config_get($k);
        }

        $this->display();
    }

}